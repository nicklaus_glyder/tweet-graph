# README #

### What is this repository for? ###

* Analyze Tweets using Kafka and SPARK, in real time
* 0.0.1

### How do I get set up? ###

* Clone the repository, and make sure you have the following installed:
* Java
* Apache Kafka
* Apache SPARK
* Maven

Run the following maven build command inside the producer and consumer directories:

mvn clean compile package

Assuming you have SPARK and Kafka installed, you can run the producer-init script to initialize Zookeeper. 
After that, you can run the producer JAR to start collecting tweets. 

Submit the consumer JAR to your SPARK scheduler (local or yarn) and pass in a configuration file similar to
cypress_config.json or local_config.json, samples provided in the consumer directory.

### Who do I talk to? ###

* Nick Glyder, Clemson University
package com.nglyder.kafka.twitter_producer;

// Based on the example found at the following sources: 
// https://dzone.com/articles/how-to-write-a-kafka-producer-using-twitter-stream
// https://kafka.apache.org/10/javadoc/index.html?org/apache/kafka/clients/producer/KafkaProducer.html

import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.ArrayList;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.log4j.BasicConfigurator;

import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

public class TwitterProducer {
	private static final String topic = "tweet-stream";
	
	public static void main(String[] args) {
		if (args[0] == null) {
			System.err.println("java -jar <jar-name> <max-tweets>");
			System.exit(1);
		}
		
		BasicConfigurator.configure();
		
		Properties properties = new Properties();
        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        
        BlockingQueue<String> queue = new LinkedBlockingQueue<String>(10000);
        StatusesFilterEndpoint endpoint = new StatusesFilterEndpoint();

        // add some track terms
        ArrayList<String> terms = new ArrayList<String>();
        terms.add("NFL");
        endpoint.trackTerms(terms);        

        String consumerKey = "7mDutNTJ2hsBw4OCQJpd7qfph";
        String consumerSecret = "unyaVPOwD1Jd3rC1fDQ7HeH5kIsC3KCAnyqHV2RlNlPo5pWbIq";
        String token = "325152729-7gDFG6pO3cdTDeALfggiZ4X6pO6GzOCTdqgY32XA";
        String secret = "m9UFGHV3r4b6ljGlNo2wMo3Ix2NlacAEGFpa6dVsnDU8s";
        
        Authentication auth = new OAuth1(consumerKey, consumerSecret, token, secret);
        Client client = new ClientBuilder().hosts(Constants.STREAM_HOST)
        						.endpoint(endpoint).authentication(auth)
        						.processor(new StringDelimitedProcessor(queue)).build();        
        
        client.connect();
        
        Producer<String, String> producer = new KafkaProducer<>(properties);
        
        // Do whatever needs to be done with messages
        for (int msgRead = 0; msgRead < Integer.parseInt(args[0]); msgRead++) {
        	ProducerRecord<String, String> message = null;
        	try {
        		message = new ProducerRecord<String, String>(topic, queue.take());
        	} catch (InterruptedException e) {
        		e.printStackTrace();
        	}
        	producer.send(message);
        }
        
        producer.close();
        client.stop();        
	}

}

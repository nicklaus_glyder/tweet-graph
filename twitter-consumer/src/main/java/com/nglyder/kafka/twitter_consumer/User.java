package com.nglyder.kafka.twitter_consumer;

import java.io.Serializable;

import scala.Tuple2;

public class User implements Serializable{
	Long id;
	String screen_name;

	public static User fromTuple(Tuple2<Long, String> tuple) {
		return new User(tuple._1(), tuple._2());
	}
	
	public User(Long id, String screen_name) {
		this.id = id;
		this.screen_name = screen_name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getScreen_name() {
		return screen_name;
	}

	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}
	
}

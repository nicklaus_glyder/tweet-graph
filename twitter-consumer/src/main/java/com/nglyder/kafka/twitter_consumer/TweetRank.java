package com.nglyder.kafka.twitter_consumer;

/**
 * Bean for rankings
 */
public class TweetRank implements java.io.Serializable {
	Long id;
	String text;
	Double pagerank;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public Double getPagerank() {
		return pagerank;
	}
	
	public void setPagerank(Double pagerank) {
		this.pagerank = pagerank;
	}
	
}

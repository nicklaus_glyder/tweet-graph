package com.nglyder.kafka.twitter_consumer;

import java.util.ArrayList;

import com.google.gson.JsonObject;

import scala.Tuple2;

/**
 * Because this class must be serializable, we can't have circular references
 * So, if you want to recover retweets or quotes, make the root tweet first
 * and then parse the retweet or quote while you are streaming the JSON
 **/
public class Tweet implements java.io.Serializable {
	private Long id;
	private String text;
	private Tuple2<Long, String> user;
	private Long retweeted_status_id = null;
	private Long quoted_status_id = null;
	private ArrayList<Tuple2<Long, String>> user_mentions;
	private ArrayList<String> hashtags;
    
	public Tweet(JsonObject data) {
		this.id = data.get("id").getAsLong();
		this.text = data.get("text").getAsString();
		
		JsonObject userJson = data.get("user").getAsJsonObject();
		this.user = new scala.Tuple2<Long, String>(
				 userJson.get("id").getAsLong(), 
				 userJson.get("screen_name").getAsString());
		
		if (data.get("retweeted_status") != null) {
			this.retweeted_status_id = data.get("retweeted_status")
										   .getAsJsonObject()
										   .get("id").getAsLong();
		}
		
		if (data.get("quoted_status") != null) {
			this.quoted_status_id = data.get("quoted_status")
										   .getAsJsonObject()
										   .get("id").getAsLong();
		}
		
		this.user_mentions = new ArrayList<Tuple2<Long, String>>();
		this.hashtags = new ArrayList<String>();
		
		data.get("entities").getAsJsonObject().get("user_mentions").getAsJsonArray().forEach((mention) -> {
			JsonObject mentionJson = mention.getAsJsonObject();
			this.user_mentions.add(new Tuple2<Long, String>(mentionJson.get("id").getAsLong(), 
											mentionJson.get("screen_name").getAsString()));
		});
		
		data.get("entities").getAsJsonObject().get("hashtags").getAsJsonArray().forEach((hashtag) -> {
			this.hashtags.add(hashtag.getAsJsonObject().get("text").getAsString());
		});
		
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public Tuple2<Long, String> getUser() {
		return user;
	}

	public void setUser(Tuple2<Long, String> user) {
		this.user = user;
	}

	public Long getRetweeted_status_id() {
		return retweeted_status_id;
	}

	public void setRetweeted_status_id(Long retweeted_status_id) {
		this.retweeted_status_id = retweeted_status_id;
	}

	public Long getQuoted_status_id() {
		return quoted_status_id;
	}

	public void setQuoted_status_id(Long quoted_status_id) {
		this.quoted_status_id = quoted_status_id;
	}
	
	public ArrayList<Tuple2<Long, String>> getUser_mentions() {
		return user_mentions;
	}

	public void setUser_mentions(ArrayList<Tuple2<Long, String>> user_mentions) {
		this.user_mentions = user_mentions;
	}

	public ArrayList<String> getHashtags() {
		return hashtags;
	}

	public void setHashtags(ArrayList<String> hashtags) {
		this.hashtags = hashtags;
	}
}

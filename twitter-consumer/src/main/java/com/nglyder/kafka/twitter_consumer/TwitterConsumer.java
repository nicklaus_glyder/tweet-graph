package com.nglyder.kafka.twitter_consumer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.*;
import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;
import org.graphframes.GraphFrame;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import scala.collection.Seq;

// --------------------------------------------------------------------------------------
// Based on the examples found at the following sources: 
// https://dzone.com/articles/how-to-write-a-kafka-producer-using-twitter-stream
// https://kafka.apache.org/10/javadoc/index.html?org/apache/kafka/clients/producer/KafkaProducer.html
// Hooking up GraphFrames to a Kafka consumer is somewhat novel
// --------------------------------------------------------------------------------------

public class TwitterConsumer {	
	// Maintain two Graphs, one of Tweets and one of Users
	private static Dataset<Row> TWEET_NODES;
	private static Dataset<Row> TWEET_EDGES;
	private static Dataset<Row> USER_NODES;
	private static Dataset<Row> USER_EDGES;
	private static Dataset<Row> TIME_PROFILE;
	private static Encoder<TweetRank> TWEET_RANK_ENCODER;
	private static Encoder<UserRank> USER_RANK_ENCODER;
	private static SparkSession SESSION;
	private static int TWEET_COUNT = 0;
	/**
	 * Erase old results if any, start Kafka consumer
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// Check for Bootstrap to zookeeper
			if (args.length != 1) {
				System.err.println("ERROR: spark-submit <name-of-jar> <config>");
				System.exit(1);
			}
			
//			deleteFilesForPathByPrefix(Paths.get("./"), "userPageRank_");
//			deleteFilesForPathByPrefix(Paths.get("./"), "tweetPageRank_");
			
			File config = new File(args[0]);
			JsonObject configJson = null;
			
			if (config.exists()) {
				configJson = new JsonParser().parse(new FileReader(config)).getAsJsonObject();
			} else {
				System.err.println("Must provide valid config file");
				System.exit(1);
			}
			
			// Setup SPARK session and DataFrames
			SESSION = SparkSession.builder().appName("SPARK Tweet Processing").getOrCreate();
			TWEET_NODES = SESSION.createDataFrame(Collections.EMPTY_LIST, Tweet.class);
			TWEET_EDGES = SESSION.createDataFrame(Collections.EMPTY_LIST, RelationalEdge.class);
			USER_NODES = SESSION.createDataFrame(Collections.EMPTY_LIST, User.class);
			USER_EDGES = SESSION.createDataFrame(Collections.EMPTY_LIST, RelationalEdge.class);
			TIME_PROFILE = SESSION.createDataFrame(Collections.EMPTY_LIST, TimeProfile.class);
			TWEET_RANK_ENCODER = Encoders.bean(TweetRank.class);
			USER_RANK_ENCODER = Encoders.bean(UserRank.class);					
			
			// Create Kafka Consumer
			final Properties props = new Properties();
			props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, configJson.get("bootstrap").getAsString());
			props.put(ConsumerConfig.GROUP_ID_CONFIG, configJson.get("group_id").getAsString());
		    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, configJson.get("enable_auto_commit").getAsString());
		    props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, configJson.get("auto_commit_interval").getAsString());
			props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, configJson.get("max_poll").getAsString());
			props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
			props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
			
			final Consumer<String, String> consumer = new KafkaConsumer<>(props);
			
			Collection<String> TOPIC = Collections.singletonList(configJson.get("topic").getAsString());
			consumer.subscribe(TOPIC);	
			
			processTweets(consumer, configJson.get("batch_size").getAsInt());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	

	/**
	 * Deletes directories along the given Path with the given prefix
	 * Used to cleanup SPARK pagerank results
	 * @param path
	 * @param prefix
	 */
	public static void deleteFilesForPathByPrefix(final Path path, final String prefix) {
	    try (DirectoryStream<Path> newDirectoryStream = Files.newDirectoryStream(path, prefix + "*")) {
	        for (final Path newDirectoryStreamItem : newDirectoryStream) {
	        	FileUtils.deleteDirectory(newDirectoryStreamItem.toFile()); 
	        }
	    } catch (final Exception e) { // empty
	    }
	}		
	
	/**
	 * Here we parse tweets in batches as they are produced/consumed. 
	 * Each batch will be used to update the graph DataFrames...
	 * We don't need to run the algorithms every time, and this part is serial anyway...
	 * So we'll keep a running count and process every X tweets that come in.
	 * @throws IOException 
	 */
	private static void processTweets(Consumer<String, String> tweetConsumer, int batch_size) throws IOException {
		// Buffers/Queues to collect tweets in. Allows us to move them to Spark in batches
		List<Tweet> tweets = new ArrayList<Tweet>();
		List<User> users = new ArrayList<User>();
		List<RelationalEdge> tweet_edges = new ArrayList<RelationalEdge>();
		List<RelationalEdge> user_edges = new ArrayList<RelationalEdge>();

		while (true) {
			ConsumerRecords<String, String> kafkaRecords = tweetConsumer.poll(1);		
			
			if (kafkaRecords.count() > 0) {
				for (ConsumerRecord<String, String> record : kafkaRecords) {
					// Parse and add the tweet + user
					JsonObject tweetJson = new JsonParser().parse(record.value()).getAsJsonObject();
					Tweet t = new Tweet(tweetJson);			
					tweets.add(t);
					users.add(User.fromTuple(t.getUser()));
					
					// Parse the retweeted status if any
					if (t.getRetweeted_status_id() != null) {
						Tweet rt = new Tweet(tweetJson.get("retweeted_status").getAsJsonObject());			
						tweets.add(rt);
						tweet_edges.add(new RelationalEdge("retweet", t.getId(), rt.getId()));
						users.add(User.fromTuple(rt.getUser()));
						user_edges.add(new RelationalEdge("retweet", t.getUser()._1(), rt.getUser()._1()));
					}
					
					// parse the quote if any
					if (t.getQuoted_status_id() != null) {
						Tweet qt = new Tweet(tweetJson.get("quoted_status").getAsJsonObject());
						tweets.add(qt);
						tweet_edges.add(new RelationalEdge("quote", t.getId(), qt.getId()));		
						users.add(User.fromTuple(qt.getUser()));
						user_edges.add(new RelationalEdge("quote", t.getUser()._1(), qt.getUser()._1()));
					}
					
					// Add all in-text mentions to the User graph
					t.getUser_mentions().forEach((user) -> {
						users.add(User.fromTuple(user));
						user_edges.add(new RelationalEdge("mention", t.getUser()._1(), user._1()));
					});
				}
				
				// Only process the batch if we have at least X tweets
				if (tweets.size() >= batch_size) {
					TWEET_COUNT += batch_size;
					
					// Update the DataFrames with nodes and edges
					long updateStartTime = System.nanoTime();
					updateTweetGraph(tweets, tweet_edges);
					updateUserGraph(users, user_edges);	
					pageRankUsersItr(1);
					long updateEndTime = System.nanoTime();
					long updateDuration = (updateEndTime - updateStartTime) / 1000000;
					
					updateTimeProfile(TWEET_COUNT, updateDuration);
					
					tweets.clear();
					users.clear();
					tweet_edges.clear();
					user_edges.clear();
				}	
			}
		}
	}
	
	/**
	 * 
	 * @param tweets
	 * @param tweet_edges
	 * 
	 * Add the given lists to the DataFrame used to generate the Tweet Graph
	 * Only has connections for Retweets and Quotes right now, but can be easily extended 
	 * depending on the connection model
	 */
	private static void updateTweetGraph(List<Tweet> tweets, List<RelationalEdge> tweet_edges) {
		Dataset<Row> nodeDF = SESSION.createDataFrame(tweets, Tweet.class);
		TWEET_NODES = TWEET_NODES.union(nodeDF).distinct();
		
		Dataset<Row> edgeDF = SESSION.createDataFrame(tweet_edges, RelationalEdge.class);
		TWEET_EDGES = TWEET_EDGES.union(edgeDF);
	}
	
	/**
	 * 
	 * @param users
	 * @param user_edges
	 * 
	 * Add the given lists to the DataFrame for the user graph. Connections can either be
	 * mentions, retweets, or quotes. This graph is suitable for identifying important users, 
	 * and can be joined into the Tweet graph based on shared id
	 */
	private static void updateUserGraph(List<User> users, List<RelationalEdge> user_edges) {
		Dataset<Row> nodeDF = SESSION.createDataFrame(users, User.class);
		USER_NODES = USER_NODES.union(nodeDF).distinct();

		Dataset<Row> edgeDF = SESSION.createDataFrame(user_edges, RelationalEdge.class);
		USER_EDGES = USER_EDGES.union(edgeDF);
	}
	
	/**
	 * Output timing metrics
	 * @param count
	 * @param time
	 */
	private static void updateTimeProfile(int count, long time) {
		TimeProfile tp = new TimeProfile(count, time);
		List<TimeProfile> tpl = Collections.singletonList(tp);
		Dataset<Row> timeDF = SESSION.createDataFrame(tpl, TimeProfile.class);
		TIME_PROFILE = TIME_PROFILE.union(timeDF);
		
		String filename = "timeProfile_"+TWEET_COUNT+".csv";
		
		TIME_PROFILE.coalesce(1).write()
			 .option("header", "true")
			 .option("delimiter", ",")
			 .csv(filename);
	}
	
	@SuppressWarnings("unused")
	private static void pageRankTweetsItr(int num_itr) {
		GraphFrame G = new GraphFrame(TWEET_NODES, TWEET_EDGES);
		GraphFrame result = G.pageRank().maxIter(num_itr).run();
	
	    Seq<String> joinCols = scala.collection.JavaConverters.asScalaIteratorConverter(
	            Arrays.asList("id", "text").iterator()
	        ).asScala().toSeq();
		
		Dataset<TweetRank> top_5 = result.vertices().join(USER_NODES, joinCols)
						           .select("id", "screen_name", "pagerank")
						           .orderBy(col("pagerank").desc())
						           .limit(5).as(TWEET_RANK_ENCODER);
		
		String filename = "tweetPageRank_"+TWEET_COUNT+".csv";
		
		top_5.coalesce(1).write()
			 .option("header", "true")
			 .option("delimiter", ",")
			 .csv(filename);
	}
	
	@SuppressWarnings("unused")
	private static void pageRankUsersItr(int num_itr) {
		GraphFrame G = new GraphFrame(USER_NODES, USER_EDGES);
		GraphFrame result = G.pageRank().maxIter(num_itr).run();

	    Seq<String> joinCols = scala.collection.JavaConverters.asScalaIteratorConverter(
	            Arrays.asList("id", "screen_name").iterator()
	        ).asScala().toSeq();
		
		Dataset<UserRank> top_5 = result.vertices().join(USER_NODES, joinCols)
						           .select("id", "screen_name", "pagerank")
						           .orderBy(col("pagerank").desc())
						           .limit(5).as(USER_RANK_ENCODER);
		
		String filename = "userPageRank_"+TWEET_COUNT+".csv";
		
		top_5.coalesce(1).write()
			 .option("header", "true")
			 .option("delimiter", ",")
			 .csv(filename);
	}
	
}

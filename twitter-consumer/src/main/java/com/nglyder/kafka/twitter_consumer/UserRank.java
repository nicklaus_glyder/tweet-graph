package com.nglyder.kafka.twitter_consumer;

import java.io.Serializable;

import scala.Tuple3;

/**
 * Bean for user ranks
 */
public class UserRank implements Serializable{
	Long id;
	String screen_name;
	Double pagerank;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getScreen_name() {
		return screen_name;
	}
	
	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}
	
	public Double getPagerank() {
		return pagerank;
	}
	
	public void setPagerank(Double pagerank) {
		this.pagerank = pagerank;
	}
}

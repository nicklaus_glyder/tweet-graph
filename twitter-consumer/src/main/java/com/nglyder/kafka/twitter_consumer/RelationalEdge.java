package com.nglyder.kafka.twitter_consumer;

import java.io.Serializable;

public class RelationalEdge implements Serializable {
	private String relation;
	private Long src;
	private Long dst;
	
	public RelationalEdge (String relation, Long src, Long dst) {
		this.relation = relation;
		this.src = src;
		this.dst = dst;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public Long getSrc() {
		return src;
	}

	public void setSrc(Long src) {
		this.src = src;
	}

	public Long getDst() {
		return dst;
	}

	public void setDst(Long dst) {
		this.dst = dst;
	}

}
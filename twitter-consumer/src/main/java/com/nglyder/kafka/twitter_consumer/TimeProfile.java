package com.nglyder.kafka.twitter_consumer;

public class TimeProfile {
	Integer tweet_count;
	Long time_to_process;
	
	public TimeProfile(Integer count, Long time) {
		this.tweet_count = count;
		this.time_to_process = time;
	}
	
	public Integer getTweet_count() {
		return tweet_count;
	}
	public void setTweet_count(Integer tweet_count) {
		this.tweet_count = tweet_count;
	}
	public Long getTime_to_process() {
		return time_to_process;
	}
	public void setTime_to_process(Long time_to_process) {
		this.time_to_process = time_to_process;
	}
	
	
}
